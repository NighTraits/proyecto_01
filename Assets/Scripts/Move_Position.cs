using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move_Position : MonoBehaviour
{
    Rigidbody rb;
    public float m_Speed = 5f;

    public GameObject bala;
    public GameObject bulletEmissor;

    // Start is called before the first frame update
    void Start()
    {
        //Fetch the Rigidbody from the GameObject with this script attached
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        //Store user input as a movement vector
        Vector3 m_Input = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

        //Apply the movement vector to the current position, which is
        //multiplied by deltaTime and speed for a smooth MovePosition
        rb.MovePosition(transform.position + m_Input * Time.deltaTime * m_Speed);
        // Salto?
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log("aprentando espacio");
            rb.AddForce(new Vector3(0f, 5f, 0f), ForceMode.Impulse);
        }
        //shot bullet
        if (Input.GetKeyDown(KeyCode.F))
        {
            Instantiate(bala, bulletEmissor.transform.position, bulletEmissor.transform.rotation);
        }    
    }
}
