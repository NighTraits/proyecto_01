using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float velocidad;
    private Rigidbody rb;
    private float h;
    private float v;
    
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        h = Input.GetAxis("Horizontal");
        v = Input.GetAxis("Vertical");
        Vector3 pos = new Vector3(h, 0, v);
        rb.MovePosition(pos + transform.position * velocidad * Time.deltaTime);
       // Salto?
        if (Input.GetKeyDown("space"))
        {
            print("space key was pressed");
        }
    }
}
